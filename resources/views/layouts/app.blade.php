<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{--    @stack('styles')--}}

</head>

<body>

@include('partials.header')

<main>
    @yield('content')
</main>

@include('partials.footer')

<script src="{{asset('js/app.js')}}" ></script>


</body>

</html>
