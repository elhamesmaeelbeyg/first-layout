

<header class="navbar navbar-expand-lg navbar-light bg-white border-bottom">

    <div id="RegisterLogin" class="modal fade" role="dialog"  >
        <div class="modal-dialog modal-lg" >

            <!-- Modal content-->
            <div class="modal-content " >
                <!-- <div class="modal-header" style="direction: rtl;">
                  <h4 class="modal-title ">درخواست استعلام قیمت</h4>


                </div> -->
                <div class="modal-body" style="display: flex;justify-content: space-between;">
                    <div id="register" style="background-color: lightgoldenrodyellow; width: 380px;padding: 20px;">
                        <form action="/action_page.php">
                            <div class="form-group">
                                <label for="email">ایمیل</label>
                                <input type="email" class="form-control" id="email">
                            </div>
                            <div class="form-group">
                                <label for="pwd">رمز عبور</label>
                                <input type="password" class="form-control" id="pwd">
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox"> مرا به خاطر بسپار</label>
                            </div>
                            <button type="submit" class="btn btn-warning btn-block" style="margin-top: 227px;">ثبت</button>
                        </form>
                    </div>
                    <div id="login" style="background-color: lightgoldenrodyellow;width: 380px;padding: 20px;">
                        <div class="contact-form">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="fname">نام</label>
                                <div class="">
                                    <input type="text" class="form-control" id="fname" placeholder="" name="fname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="lname">نام خانوادگی</label>
                                <div class="">
                                    <input type="text" class="form-control" id="lname" placeholder="" name="lname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label " for="lname">رمز عبور</label>
                                <div class="">
                                    <input type="text" class="form-control" id="lname" placeholder="" name="lname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label " for="lname">تکرار رمز عبور</label>
                                <div class="">
                                    <input type="text" class="form-control" id="lname" placeholder="" name="lname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label " for="email">ایمیل</label>
                                <div class="">
                                    <input type="email" class="form-control" id="email" placeholder="" name="email">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 ">
                                    <button type="submit" class="btn btn-warning btn-block">ثبت</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer" style="text-align: right;direction: rtl;">
                  <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
                </div> -->
            </div>

        </div>
    </div>

    <div class="container">
        <a class="navbar-brand" href="#">خدمت از ما</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <button type="button" class="btn btn-outline-dark mr-3 px-4">شیراز</button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#" id="home">خانه <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item mx-4 active">
                    <a class="nav-link" href="#">وبلاگ</a>
                </li>
                <li class="nav-item mx-4 active">
                    <a class="nav-link" href="#">پیوستن متخصصین</a>
                </li>
                <li class="nav-item mx-4 active">
                    <a class="nav-link" href="#">پیگیری سفارش</a>
                </li>
                <li class="nav-item mx-4 active">
                    <!-- <a class="nav-link" href="#">پیگیری سفارش</a> -->
                    <button type="button" class="btn btn-outline-dark " style="margin-left: -22px;" data-toggle="modal" data-target="#RegisterLogin" >ورود / ثبت نام</button>
                </li>

            </ul>

        </div>
    </div>

</header>
