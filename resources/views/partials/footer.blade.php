

<footer class="bg-dark container-fluid mt-5 text-secondary p-0" style="font-size: 16px;">

    <div class="container-fluid border-bottom border-secondary py-4">
        <div class="container">
            <div class="row bg-dark  border-secondary pt-3" style="min-height: 250px;">
                <div class="col-md-4  ">
                    <div>
                        <img src="{{asset('images/khedmatazma.png')}}" style="width: 230px;height: 130px;" alt="">
                    </div>
                    <div class="row my-3 text-center">
                        <div class="col-1">
                            <a href="">
                                <i class="fa fa-whatsapp text-secondary" aria-hidden="true" style="font-size: 19px;"></i>

                            </a>
                        </div>
                        <div class="col-1">
                            <a href="">
                                <i class="fa fa-twitter text-secondary" aria-hidden="true" style="font-size: 19px;"></i>
                            </a>
                        </div>
                        <div class="col-1">
                            <a href="">
                                <i class="fa fa-instagram text-secondary" aria-hidden="true" style="font-size: 19px;"></i>
                            </a>
                        </div>
                        <div class="col-1">
                            <a href="">
                                <i class="fa fa-telegram text-secondary" aria-hidden="true" style="font-size: 19px;"></i>
                            </a>
                        </div>
                        <div class="col-1">
                            <a href="">
                                <i class="fa fa-google-plus text-secondary" aria-hidden="true" style="font-size: 19px;"></i>
                            </a>
                        </div>

                    </div>
                    <div class="row mt-2">
                        <div class="col-2">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="text-warning bi bi-geo-alt" fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                            </svg>
                        </div>
                        <div class="col-10">تهران خیابان نیاوران شرقی </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-2">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="text-warning bi bi-telephone-fill"
                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2.267.98a1.636 1.636 0 0 1 2.448.152l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z" />
                            </svg>
                        </div>
                        <div class="col-10">071-3234567 </div>
                    </div>
                </div>
                <div class="col-md-2  ">
                    <div>خدمت از ما </div>
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">وبلاگ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">درباره ی ما</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">ارتباط با ما</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">تیم</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">سوالات متداول</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">قوانین و مقررات</a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="col-md-2 ">
                    <div>خدمات یار</div>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-secondary" href="#">کسب درامد</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 ">
                    <div>عضویت در خبرنامه</div>
                    <div class="mt-3">برای اطلاع از تخفیفات در خبرنامه عضو شوید</div>
                    <div class="mt-1">
                        <div class="input-group mb-3" style="direction: ltr;">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-primary" id="basic-addon1">ثبت</span>
                            </div>
                            <input type="text" style="direction: rtl;" class="form-control" placeholder="ایمیل خود را وارد کنید" aria-label="Username" aria-describedby="basic-addon1">
                        </div>

                    </div>
                    <div class="mt-3">دریافت لینک دانلود اپلیکیشن</div>
                    <div class="mt-1">
                        <div class="input-group mb-3" style="direction: ltr;">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-primary" id="basic-addon1">ثبت</span>
                            </div>
                            <input type="text" style="direction: rtl;" class="form-control" placeholder="شماره تلفن همراه خود را وارد کنید" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </div>
                    <div class="mt-3">دانلود اپلیکیشن</div>
                    <div class="row mt-2">
                        <div class="col-6">
                            <a href="">
                                <img src="{{asset('images/appstore.png')}}" width="130px" height="30px" alt="">
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="">
                                <img src="{{asset('images/googleplay.png')}}" width="130px" height="30px" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid py-4">
        <div class="container">
            <div class="row bg-dark pt-3 " style="min-height: 250px;">
                <div class="text-white col-12">شهر های استان فارس در خدمت از ما</div>
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شیراز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">فسا</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نیریز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">جهرم</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">داراب</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">کازرون</a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شیراز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">فسا</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نیریز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">جهرم</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">داراب</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">کازرون</a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شیراز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">فسا</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نیریز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">جهرم</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">داراب</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">کازرون</a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شیراز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">فسا</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نیریز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">جهرم</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">داراب</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">کازرون</a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شیراز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">فسا</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نیریز</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">جهرم</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">داراب</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">کازرون</a>
                            </li>
                        </ul>

                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid py-4" style="background-color: #272727;">
        <div class="container">
            <div class="row" style="min-height: 250px;">
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-white" href="#">نظافت</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نظافت منزل</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نظافت راه پله</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">مشاع</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شسشتشوی مبلمان</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شستشوی پرده</a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-white" href="#">تزیینات</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نقاشی ساختمان</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نصب کاغذ دیواری</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نصب پارکت و کف پوش</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نصب لمینت</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">بیشتر</a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-white" href="#">نظافت</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نظافت منزل</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نظافت راه پله</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">مشاع</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شسشتشوی مبلمان</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شستشوی پرده</a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-white" href="#">تزیینات</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نقاشی ساختمان</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نصب کاغذ دیواری</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نصب پارکت و کف پوش</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نصب لمینت</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">بیشتر</a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class="col-md-2 col-sm-4">
                    <nav class="navbar ">

                        <!-- Links -->
                        <ul class="navbar-nav ">
                            <li class="nav-item">
                                <a class="nav-link text-white" href="#">نظافت</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نظافت منزل</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">نظافت راه پله</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">مشاع</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شسشتشوی مبلمان</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-secondary" href="#">شستشوی پرده</a>
                            </li>
                        </ul>

                    </nav>
                </div>
            </div>
        </div>
    </div>




    <div class="p-2 text-center" style="background-color:black ;">
        <p>
            کلیه ی حقوق کپی رایت محفوظ است !
        </p>
    </div>






</footer>
