
@extends('layouts.app')



{{--@push('styles')--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
{{--@endpush--}}

@section('content')
    <div class="container-fluid bg-white py-2 text-center">
        <div class="container">
            <div class="row" id="searchForm" style="">
                <div class="col-lg-2 col-8  mt-1"><input type="text" class="form-control bg-light text-dark  "
                                                         placeholder="منطقه"></div>
                <div class="col-lg-2 col-8 mt-1"><input type="text" class="form-control bg-light text-dark "
                                                        placeholder="قیمت از"></div>
                <div class="col-lg-2 col-8 mt-1"><input type="text" class="form-control bg-light text-dark "
                                                        placeholder="قیمت تا"></div>
                <div class="col-lg-2 col-8 mt-1"><input type="text" class="form-control bg-light text-dark " placeholder="جنسیت">
                </div>
                <div class="col-lg-2 col-8 mt-1"><button class="btn btn-warning px-5"
                                                         style="">جستجو</button></div>
            </div>
        </div>
    </div>

    <div class="container pt-4">
        <div class="row">
            <section class="col-md-9">

                <div class="row">


                    <!-- ************************************************************** -->
                    <div class="col-lg-6 col-12">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>


                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price" data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <!-- ************************************************************** -->
                    <div class="col-lg-6 col-12">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 mt-4">
                        <div class="card ">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <div>
                                        <img src="{{asset('images/index.jpg')}}" alt="" style="width: 120px;height: 120px;">
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">نظافت</span>
                                            <span class="border border-dark rounded p-2">لوله کشی</span>

                                        </div>
                                        <div class="mt-3 services">
                                            <span class="border border-dark rounded p-2">کارواش</span>
                                            <span class="border border-dark rounded p-2">برق کاری</span>

                                        </div>
                                        <div class="mt-4" style="display: flex;">
                                            <div class="">
                                                <svg width="3em" height="3em" viewBox="0 0 16 16" class="text-success bi bi-ui-checks"
                                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M7 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M2 1a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2zm0 8a2 2 0 0 0-2 2v2a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-2a2 2 0 0 0-2-2H2zm.854-3.646l2-2a.5.5 0 1 0-.708-.708L2.5 4.293l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0zm0 8l2-2a.5.5 0 0 0-.708-.708L2.5 12.293l-.646-.647a.5.5 0 0 0-.708.708l1 1a.5.5 0 0 0 .708 0z" />
                                                    <path
                                                        d="M7 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-1z" />
                                                    <path fill-rule="evenodd"
                                                          d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                </svg>
                                            </div>
                                            <div class="" style="font-size: 12px;margin-right: 15px;"><b>571</b> خدمات موفق</div>
                                        </div>
                                    </div>
                                    <div style="margin-right: 15px;">

                                        <h3>طاها رضازاده</h3>
                                        <div class="mt-3" style="display: flex;">
                                            <div class="ml-2"><svg style="margin-top:-12px" width="1.3em" height="1.2em"
                                                                   viewBox="0 0 16 16" class="text-success bi bi-emoji-laughing" fill="currentColor"
                                                                   xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path fill-rule="evenodd"
                                                          d="M12.331 9.5a1 1 0 0 1 0 1A4.998 4.998 0 0 1 8 13a4.998 4.998 0 0 1-4.33-2.5A1 1 0 0 1 4.535 9h6.93a1 1 0 0 1 .866.5z" />
                                                    <path
                                                        d="M7 6.5c0 .828-.448 0-1 0s-1 .828-1 0S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 0-1 0s-1 .828-1 0S9.448 5 10 5s1 .672 1 1.5z" />
                                                </svg></div>
                                            <div style="width: 120px;" class="ml-3">
                                                <div class="progress"
                                                >
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar"
                                                         style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="" style="font-size: 8px;">75% راضی</div>
                                        </div>
                                        <div class="row my-2" style="margin-right: 0px;">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star text-warning"
                                                       fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                      d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-star-fill text-warning"
                                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>

                                        <span style="font-size: 12px;">درباره:</span>
                                        <p style="font-size: 12px;">این یک متن آزمایشی است .این یک متن آزمایشی است .این یک متن آزمایشی است
                                            .این یک متن آزمایشی است .این یک متن آزمایشی است .</p>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-left">
                                <a href="">مشاهده پروفایل</a>
                                <a href="#" class="btn btn-primary mr-2 request-price"  data-toggle="modal" data-target="#requestPriceModal">درخواست استعلام قیمت</a>

                            </div>
                        </div>
                    </div>



                </div>


                <div id="requestPriceModal" class="modal fade" role="dialog" >
                    <div class="modal-dialog" >

                        <!-- Modal content-->
                        <div class="modal-content " >
                            <div class="modal-header" style="direction: rtl;">
                                <h4 class="modal-title ">درخواست استعلام قیمت</h4>


                            </div>
                            <div class="modal-body">
                                <p>مبلغ 100000 تومان</p>
                            </div>
                            <div class="modal-footer" style="text-align: right;direction: rtl;">
                                <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
                            </div>
                        </div>

                    </div>




            </section>
            <aside class="col-md-3 rounded   text-center" style="height: 545px;">
                <ul class="nav flex-column border bg-white text-dark">
                    <li class="nav-item">
                        <span class="nav-link active bg-primary text-white" href="#">همه خدمات</span>
                    </li>
                    <li class="nav-item border-bottom p-2">
                        <a class="nav-link" href="#">نظافت</a>
                    </li>
                    <li class="nav-item border-bottom p-2">
                        <a class="nav-link" href="#">تاسیسات</a>
                    </li>
                    <li class="nav-item border-bottom p-2">
                        <a class="nav-link" href="#">باغبانی </a>
                    </li>
                    <li class="nav-item border-bottom p-2">
                        <a class="nav-link" href="#">الکتریکی</a>
                    </li>
                    <li class="nav-item border-bottom p-2">
                        <a class="nav-link" href="#">تعمیر مبلمان</a>
                    </li>
                    <li class="nav-item border-bottom p-2">
                        <a class="nav-link" href="#">دکوراسیون</a>
                    </li>
                    <li class="nav-item border-bottom p-2">
                        <a class="nav-link" href="#">پرستاری</a>
                    </li>
                    <li class="nav-item border-bottom p-2">
                        <a class="nav-link" href="#">حمل بار</a>
                    </li>
                    <li class="nav-item border-bottom p-2">
                        <a class="nav-link" href="#">سمپاشی</a>
                    </li>

                </ul>
            </aside>
        </div>
    </div>
@endsection
